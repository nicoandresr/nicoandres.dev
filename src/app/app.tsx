import styled from '@emotion/styled';
import { Route, Routes } from 'react-router-dom';
import { Home } from '../components/home';
import { Banner } from '@nicoandres.dev/ui'

const StyledApp = styled.div`
  display: flex;
  flex-direction: column;
`;

export function App() {
  return (
    <StyledApp>
      <Banner text="Nicoandres Rodriguez" />
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </StyledApp>
  );
}

export default App;
