import { render } from '@testing-library/react';
import {MemoryRouter} from 'react-router-dom';

import App from './app';

describe('App', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<App />, { wrapper: MemoryRouter });
    expect(baseElement).toBeTruthy();
  });

  it('should have a greeting as the title', () => {
    const { getAllByText } = render(<App />, { wrapper: MemoryRouter });
    expect(getAllByText(/Nicoandres Rodriguez/i).length === 2).toBeTruthy();
  });
});
