import { Headline } from "@nicoandres.dev/ui";
import Link from '@mui/material/Link';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';

import Container from '@mui/material/Container';

export function Home() {
	const techStack = ["React - Typescript", "NextJS", "NestJS", "Playwright"];
	return (
		<Container>
		<Headline variant="h1">
			Nicoandres Rodriguez
		</Headline>
		<Headline variant="h2">
			FullStack Developer
			<br />
			Based on Helsinki
			<br />
		</Headline>
			<br />
		<Headline variant="h5">
			Tech Stack:
		</Headline>
			<List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
				<ListItem>
					<ListItemText primary="React - Typescript" />
				</ListItem>
				<ListItem>
					<ListItemText primary="Playwright" />
				</ListItem>
				<ListItem>
					<ListItemText primary="NextJS" />
				</ListItem>
				<ListItem>
					<ListItemText primary="NestJS" />
				</ListItem>
				<ListItem>
					<ListItemText primary="Mongo" />
				</ListItem>
			</List>
		<Headline variant="h5">
				Apps:
		</Headline>
			<ul>
			<li>
			<Link href="https://apps.nicoandres.dev" >Css 3D cube</Link>
			</li>
			</ul>
			</Container>
	);
}

export default Home;
