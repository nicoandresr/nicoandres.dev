import Typography from '@mui/material/Typography';

export function Headline(props: TextProps) {
  return (
    <Typography {...props} />
  );
}

export default Headline;
