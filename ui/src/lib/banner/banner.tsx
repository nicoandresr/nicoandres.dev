import styled from '@emotion/styled';

export interface BannerProps {
  text: string;
}

const StyledBanner = styled.header`
  color: black;
`;

export function Banner(props: BannerProps) {
  return (
    <StyledBanner>
      {props.text}
    </StyledBanner>
  );
}

export default Banner;
